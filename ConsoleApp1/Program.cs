﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Entity.Core.Objects;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            // Get the current date.
            DateTime thisDay = DateTime.Today.AddYears(-20);
            // Display the date in the default (general) format.
            //Console.WriteLine(thisDay);


            //Empleados();
            //HireDate(thisDay);
            //Belgica();
            //Bruxelles();
            //London();
            //Antiguos();
            //MasAntiguosLondon();
            //MasAntiguosLondon2();
            //MismaCiudad();
            //SinOrdenes();
            //ConProd_Menora_5();
            //promedioCategoria();
            //ProvS();
            //Empleados3();
            //TotalVentas();
            //SieteProveedores();
            //TVentasSententaP();
            // CincoVecesProm();
            //GroupBy();
            GroupBy2();



        }
        #region
        private static void DelCompany()
        {
            NorthwindEntities dbContext = new NorthwindEntities();
            var deleteIndivCust =
                from cust in dbContext.Customers
                where cust.CustomerID == "98128"
                select cust;

            if (deleteIndivCust.Count() > 0)
            {
                dbContext.Customers.Remove(deleteIndivCust.First());
                dbContext.SaveChanges();
            }
        }

        private static void AddCompany()
        {
            // Northwnd inherits from System.Data.Linq.DataContext.
            NorthwindEntities dbContext = new NorthwindEntities();

            Customers cust = new Customers();
            cust.CompanyName = "SomeCompany";
            cust.City = "London";
            cust.CustomerID = "98128";
            cust.PostalCode = "55555";
            cust.Phone = "555-555-5555";
            dbContext.Customers.Add(cust);

            // At this point, the new Customer object is added in the object model.
            // In LINQ to SQL, the change is not sent to the database until
            // SubmitChanges is called.
            dbContext.SaveChanges();
        }

        public static void CompanyName()
        {
            NorthwindEntities dbContext = new NorthwindEntities();
            var companyNameQuery =
            from cust in dbContext.Customers
            where cust.City == "London"
            select cust.CompanyName;

            foreach (var customer in companyNameQuery)
            {
                Console.WriteLine(customer);
            }
        }


        public static void Empleados()
        {
            NorthwindEntities dbContext = new NorthwindEntities();
            var query =
            from E in dbContext.Employees
            where E.City == "London"
            
            select new { E.FirstName, E.LastName, E.HireDate, E.Country};

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", datos.FirstName, datos.LastName, datos.HireDate, datos.Country);
            }
        }


        public static void HireDate(DateTime dt)
        {
            NorthwindEntities dbContext = new NorthwindEntities();
            var query =
            from E in dbContext.Employees
            where E.HireDate < dt
            select new { E.FirstName, E.LastName, E.City, E.Country };

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", datos.FirstName, datos.LastName, datos.City, datos.Country);
            }
        }

        public static void Belgica()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            from E in db.Employees
            join O in  db.Orders on E.EmployeeID equals O.EmployeeID
            where O.ShipCountry == "Belgium"
            select new { O.OrderID, E.FirstName, O.CustomerID, O.ShipCity };

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", datos.OrderID, datos.FirstName, datos.CustomerID, datos.ShipCity );
            }
        }

        public static void Bruxelles()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            from E in db.Employees
            join O in db.Orders on E.EmployeeID equals O.EmployeeID
            join C in db.Customers on O.CustomerID equals C.CustomerID
            join S in db.Shippers on O.ShipVia equals S.ShipperID
            where O.ShipCity == "Bruxelles" & S.CompanyName == "Speedy Express"

            select new { O.OrderID, E.FirstName, O.CustomerID, O.ShipCity };

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", datos.OrderID, datos.FirstName, datos.CustomerID, datos.ShipCity);
            }
        }

        public static void London()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (from P in db.Products
            from OD in db.Order_Details 
            join O in db.Orders on OD.OrderID equals O.OrderID
            join E in db.Employees on O.EmployeeID equals E.EmployeeID
            join C in db.Customers on O.CustomerID equals C.CustomerID
            where E.City == "London" || C.City == "London"

             select new { P.ProductName }).Distinct();

            foreach (var datos in query)
            {
                Console.WriteLine("{0}", datos.ProductName );
            }
            Console.WriteLine("Total: " + query.Count() );
        }

        public static void Antiguos()
        {
            NorthwindEntities dbContext = new NorthwindEntities();
            var query =
            (from E in dbContext.Employees 
            orderby E.HireDate ascending
            
            select new { E.FirstName, E.LastName, E.City, E.HireDate }).Take(3)
            ;

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", datos.FirstName, datos.LastName, datos.City, datos.HireDate);
            }
        }

        public static void MasAntiguosLondon()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (from E in db.Employees
             where E.HireDate  < (from E2 in db.Employees.Where(E2 => E2.City=="London")
                                  select E2.HireDate).Min()

             select new { E.FirstName, E.LastName, E.City, E.HireDate })
            ;

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", datos.FirstName, datos.LastName, datos.City, datos.HireDate);
            }
        }

        public static void MasAntiguosLondon2()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (from E in db.Employees
             where E.HireDate < (from E2 in db.Employees
                                 where E2.City == "London"
                                 select E2.HireDate).Min()

             select new { E.FirstName, E.LastName, E.City, E.HireDate })
            ;

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", datos.FirstName, datos.LastName, datos.City, datos.HireDate);
            }
        }


        public static void MismaCiudad()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (
             
             from E in db.Employees
             from O in E.Orders
                 //join E in db.Employees on O.EmployeeID equals E.EmployeeID
             join C in db.Customers on O.CustomerID equals C.CustomerID
             where E.City ==  C.City 

             select new { E.FirstName, E.City, C.CompanyName,  Ciudad = C.City, E.Title }).Distinct();

            foreach (var datos in query)
            {
                Console.WriteLine("{0},{1},{2},{3},{4}", datos.Title, datos.FirstName, datos.City, datos.CompanyName, datos.Ciudad);
            }
            Console.WriteLine("Total: " + query.Count());
        }


        public static void SinOrdenes()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (
             //from O in db.Orders
             from C in db.Customers 
             where C.Orders.Count == 0

             select new {  C.CompanyName }).Distinct();

            foreach (var datos in query)
            {
                Console.WriteLine("{0}",  datos.CompanyName);
            }
            Console.WriteLine("Total: " + query.Count());
        }

        public static void ConProd_Menora_5()
        {
            NorthwindEntities db = new NorthwindEntities();
            int num = 5;
            var query =(
                from C in db.Customers
                let allProducts = from P in db.Products
                                  where P.UnitPrice <= num
                                  select P.ProductID
                where !allProducts.Except(
                    from O in C.Orders
                    from D in O.Order_Details
                    select D.ProductID).Any()
                select new { Cliente = C.CompanyName });          
            


            foreach (var datos in query)
            {
                Console.WriteLine("{0}", datos.Cliente);
            }
            Console.WriteLine("Total: " + query.Count());
        }

        public static void promedioCategoria()
        {

            NorthwindEntities db = new NorthwindEntities();
            var categories =
                from p in db.Products
                join c in db.Categories on p.CategoryID equals c.CategoryID
                group p by p.Categories.CategoryName into g
                
                select new
                {
                    llave = g.Key,
                    AvgPrice = g.Average(c => c.UnitPrice)
                };

            foreach (var category in categories)
            {
                Console.WriteLine("Category ID: {0}, name: {1}",
                    category.llave, category.AvgPrice);
            }

            Console.WriteLine("Total: " + categories.Count());
        }


        public static void ProvS()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (
             from P in db.Products
             from S in db.Suppliers
             where S.Products.Count() > 3

             select new { S.SupplierID, S.CompanyName }).Distinct();

            foreach (var datos in query)
            {
                Console.WriteLine("{0}", datos.CompanyName);
            }
            Console.WriteLine("Total: " + query.Count());
        }


        public static void Empleados3()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (

             from E in db.Employees
             orderby E.EmployeeID
             select new
             {
                 E.EmployeeID,
                 E.FirstName,
                 NumOrders = E.Orders.Count()

             }) ; 

            foreach (var datos in query)
            {
                Console.WriteLine("{0},{1},{2}", datos.EmployeeID, datos.FirstName,  datos.NumOrders);
            }
            Console.WriteLine("Total: " + query.Count());
        }



        public static void TotalVentas()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (

             from E in db.Employees
             orderby E.EmployeeID

             select new
             {
                 E.EmployeeID,
                 E.FirstName,
                 Ventas_Totales = (
                 from O in E.Orders
                 from D2 in db.Order_Details 
                 
                    let Tsub= System.Convert.ToDouble(D2.UnitPrice) * D2.Quantity
                 select Tsub 

                 ).Sum()
                 

             });

            foreach (var datos in query)
            {
                Console.WriteLine("{0},{1},{2}", datos.EmployeeID, datos.FirstName, datos.Ventas_Totales);
            }
            Console.WriteLine("Total: " + query.Count());
        }

        public static void SieteProveedores()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =
            (from E in db.Employees
            let nSuppliers = (from P in db.Products
                              select P.SupplierID).Distinct().Count()
            where nSuppliers >= 7
            select new { E.FirstName, E.LastName, nSuppliers});
             

            foreach (var datos in query)
            {
                Console.WriteLine("{0}, {1}, {2}", datos.FirstName, datos.LastName, datos.nSuppliers);
            }
            Console.WriteLine("total: " + query.Count());
        }


        public static void TVentasSententaP()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query =(

             from E in db.Employees
             let setentaProds = (
                from O in E.Orders
                from D in O.Order_Details
                select D.ProductID
             ).Distinct().Count()

             where setentaProds > 70
             orderby E.EmployeeID
             select new
             {
                 E.EmployeeID,
                 E.FirstName,
                 Ventas_Totales = (
                 from O in E.Orders
                 from D2 in O.Order_Details
                 let Tsub = (double)(D2.UnitPrice) * D2.Quantity
                 select Tsub

                 ).Sum()

             }
             );

            foreach (var datos in query)
            {
                Console.WriteLine("{0},{1},{2}", datos.EmployeeID, datos.FirstName, datos.Ventas_Totales);
            }
            Console.WriteLine("Total: " + query.Count());
        }


        public static void CincoVecesProm()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query = (

             from C in db.Customers
             from O in C.Orders
             from D in O.Order_Details
             join P in db.Products on D.ProductID equals P.ProductID

             let avgsales = (
             from D1 in db.Order_Details
             where D1.ProductID == P.ProductID
             select D1.Quantity
             ).Cast<int>().Average()

             where D.Quantity > avgsales * 5

             select new { C.CompanyName, P.ProductName, D.Quantity });



            foreach (var datos in query)
            {
                Console.WriteLine("{0},{1},{2}", datos.CompanyName, datos.ProductName, datos.Quantity);
            }
            Console.WriteLine("Total: " + query.Count());
        }

        #endregion


        public static void GroupBy()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query = (
            from E in db.Employees
            group E by E.Country into g
            select new
            {
                g.Key
            }
            
            );

            foreach (var datos in  query)
            {
                Console.WriteLine("Contry: {0},{1}", datos.Key, datos.Key.Count());
            }
        }

        public static void GroupBy2()
        {
            NorthwindEntities db = new NorthwindEntities();
            var query = (
            from E in db.Employees
            group E by E.Country into g
            select new
            {
                g.Key
            }

            );

            



            foreach (var datos in query)
            {
                Console.WriteLine("Contry: {0}", datos.Key);

                var query2 = (
            from E2 in db.Employees
            where E2.Country == datos.Key

            select new
            {
                E2.EmployeeID,
                E2.Country
            }

            );
                foreach (var datos2 in query2)
                {
                    Console.WriteLine("ID {0}, {1}", datos2.EmployeeID, datos2.Country);
                }

                Console.WriteLine("SubTotal: " + query2.Count() );
            }

            Console.WriteLine("Total: " + query.Count());
        }
















    }


}
